#!/bin/bash -


echo "Downloading required packages"
apt-get update
apt-get install firefox
apt-get install x11
apt-get install xorg
apt-get install gdm
apt-get install matchbox-window-manager
apt-get install rsync
apt-get install git
apt-get install can-utils
apt-get install pip3
apt-get install python3.5-dev
echo "Packages have been installed"

echo "Configuring python"
apt-get install python3.5
rm /usr/bin/python3
ln -s /usr/bin/python3.5 /usr/bin/python3
apt-get install python3-pip
echo "python configured"




echo "Moving start up scripts into place"
cp firefox_start.sh /home/scada/firefox_start.sh
cp scadad_start.sh /home/scada/scadad_start.sh
cp safetyd_start.sh /home/scada/safetyd_start.sh

echo "Adding processes and making them boot at startup"
cp firefox_kiosk.service /lib/systemd/firefox_kiosk.service
cp scadad.service /lib/systemd/scadad.service
cp safetyd.service /lib/systemd/safetyd.service
ln -s /lib/systemd/firefox_kiosk.service /etc/systemd/system/multi-user.target.wants/firefox_kiosk.service
ln -s /lib/systemd/scadad.service /etc/systemd/system/multi-user.target.wants/scadad.service
ln -s /lib/systemd/safetyd.service /etc/systemd/system/multi-user.target.wants/safetyd.service
systemctl daemon-reload
echo "Processes have been added"

"Fetching repos of scadad and scada-ui"
#make a /home/scada folder if it does not exist
if [ ! -d "/home/scada" ] ; then
	mkdir /home/scada
fi

cd /home/scada
git clone https://git.lafayette.edu/ece/ev-scadad.git
git clone https://git.lafayette.edu/ece/ev-scada-ui.git
chmod -R 777 /home/scada
echo "Repos have been downloaded to /home/scada"

echo "Disabling X11 screen sleeping"
echo xset s off >> /etc/xinit/xinitrc
echo xset -dpms >> /etc/xinit/xinitrc
echo xset s noblank >> /etc/xinit/xinitrc
cp piConfig /etc/kdb/config
echo "X11 sleeping has been disabled"

echo "SCADA has been installed!  Reboot to start SCADA operations."

